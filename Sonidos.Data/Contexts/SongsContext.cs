using Microsoft.EntityFrameworkCore;
using Sonidos.Models;

namespace Sonidos.Data.Contexts
{
    public class SongsContext : DbContext
    {
        public virtual DbSet<Song> Songs { get; set; }
        public virtual DbSet<Album> Albums { get; set; }

        public SongsContext()
        {
        }

        public SongsContext(DbContextOptions<SongsContext> options) : base(options)
        {
        }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
            => optionsBuilder.UseNpgsql("Host=localhost;Database=sonidos;Username=postgres;Password=postgres");
    }
}
