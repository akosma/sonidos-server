﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sonidos.Data.Migrations
{
    public partial class WrongType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<long>(
                name: "Size",
                table: "Songs",
                nullable: false,
                oldClrType: typeof(int));
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<int>(
                name: "Size",
                table: "Songs",
                nullable: false,
                oldClrType: typeof(long));
        }
    }
}
