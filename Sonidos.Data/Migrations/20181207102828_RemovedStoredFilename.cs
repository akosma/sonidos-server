﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sonidos.Data.Migrations
{
    public partial class RemovedStoredFilename : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "StoredFilename",
                table: "Songs");
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<string>(
                name: "StoredFilename",
                table: "Songs",
                nullable: true);
        }
    }
}
