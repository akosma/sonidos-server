﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Sonidos.Data.Migrations
{
    public partial class DisplayType : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.RenameColumn(
                name: "Filename",
                table: "Songs",
                newName: "StoredFilename");

            migrationBuilder.AddColumn<string>(
                name: "DisplayFilename",
                table: "Songs",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DisplayFilename",
                table: "Songs");

            migrationBuilder.RenameColumn(
                name: "StoredFilename",
                table: "Songs",
                newName: "Filename");
        }
    }
}
