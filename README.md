# Sonidos

This project contains the code for the following functionality:

- Users can upload MP3 files;
- MP3 file data is stored in a database;
- An API exposes the stored music and allows clients to connect and listen to it.

The project uses a PostgreSQL database. Mac users should install [Postgres.app](https://postgresapp.com/).

## Requirements

This project requires a local installation of .NET Core 2.2 as a minimum

## Getting Started

- `git clone git@gitlab.com:akosma/sonidos-server.git`
- `cd sonidos-server`
- `cd Sonidos.Data`
- `dotnet ef database update`
- `cd ../Sonidos.Admin`
- `dotnet run`

Open the browser in `http://localhost:5050` to see the application running.

