﻿using System;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;

namespace Sonidos.Tools.Extensions
{
    public static class ControllerExtensions
    {
        public static IActionResult DownloadFile(this ControllerBase controller, Guid storedFilename, string displayFilename)
        {
            IFileProvider provider = new PhysicalFileProvider("/home/akosma/Dropbox/hernun/Sonidos/sonidos-server/uploads");
            IFileInfo fileInfo = provider.GetFileInfo(storedFilename.ToString());
            var readStream = fileInfo.CreateReadStream();
            return controller.File(readStream, "audio/mpeg3", displayFilename);
        }
    }
}