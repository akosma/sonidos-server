using System.Diagnostics;

namespace Sonidos.Tools.Extensions
{
    public static class StringExtensions
    {
        /// <summary>
        /// Inspired by
        /// https://loune.net/2017/06/running-shell-bash-commands-in-net-core/
        /// and
        /// https://stackoverflow.com/a/50898285
        /// </summary>
        /// <param name="cmd">The command to be executed on the command line</param>
        /// <returns>The output of the command.</returns>
        public static string Bash(this string cmd)
        {
            var escapedArgs = cmd.Replace("\"", "\\\"");
            
            var process = new Process
            {
                StartInfo = new ProcessStartInfo
                {
                    FileName = "/bin/bash",
                    Arguments = $"-c \"{escapedArgs}\"",
                    RedirectStandardOutput = true,
                    UseShellExecute = false,
                    CreateNoWindow = true,
                }
            };
            process.Start();
            string output = process.StandardOutput.ReadToEnd();
            string error = process.StandardError.ReadToEnd();
            process.WaitForExit();
            if (string.IsNullOrEmpty(error))
            {
                return output;
            }
            return error;
        }        
    }
}