﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Sonidos.Admin.Models;
using Sonidos.Data.Contexts;
using Sonidos.Models;
using Sonidos.Tools.Extensions;

namespace Sonidos.Admin.Controllers
{
    public class HomeController : Controller
    {
        private readonly SongsContext _context;

        public HomeController(SongsContext context)
        {
            _context = context;
        }

        [Route("/")]
        [Route("index")]
        public IActionResult Index()
        {
            IEnumerable<Song> songs = _context.Songs.OrderBy(obj => obj.DisplayFilename).ToList();
            return View(songs);
        }

        [Route("privacy")]
        public IActionResult Privacy()
        {
            return View();
        }

        [Route("error")]
        [ResponseCache(Duration = 0, Location = ResponseCacheLocation.None, NoStore = true)]
        public IActionResult Error()
        {
            return View(new ErrorViewModel {RequestId = Activity.Current?.Id ?? HttpContext.TraceIdentifier});
        }

        [HttpGet]
        [Route("download/{guid}")]
        public IActionResult Download(Guid guid)
        {
            Song song = _context.Songs.FirstOrDefault(obj => obj.Id == guid);
            if (song == null)
            {
                return NotFound();
            }

            Guid storedFilename = song.Id;
            string displayFilename = song.DisplayFilename;

            return this.DownloadFile(storedFilename, displayFilename);
        }

        [HttpGet]
        [Route("upload")]
        public IActionResult Upload()
        {
            return View();
        }

        [HttpPost]
        [Route("upload")]
        public async Task<IActionResult> Upload(List<IFormFile> files)
        {
            long size = files.Sum(f => f.Length);

            foreach (var formFile in files)
            {
                if (formFile.Length > 0)
                {
                    Song song = new Song
                    {
                        DisplayFilename = formFile.FileName,
                        Size = size
                    };
                    _context.Add(song);
                    string path = $"/home/akosma/Dropbox/hernun/Sonidos/sonidos-server/uploads/{song.Id}";
                    using (var stream = new FileStream(path, FileMode.Create))
                    {
                        await formFile.CopyToAsync(stream);
                    }
                }
            }

            await _context.SaveChangesAsync();

            return RedirectToAction(nameof(Index));
        }
    }
}
