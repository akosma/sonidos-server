﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.HttpsPolicy;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Versioning;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Sonidos.Data.Contexts;
using Swashbuckle.AspNetCore.Swagger;

namespace Sonidos.API.V1
{
    /// <summary>
    /// Encapsulates startup configuration
    /// </summary>
    public class Startup
    {
        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="configuration">Configuration object passed at startup</param>
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        /// <summary>
        /// Configuration object
        /// </summary>
        public IConfiguration Configuration { get; }

        /// <summary>
        /// This method gets called by the runtime. Use this method to add services to the container.
        /// </summary>
        /// <param name="services">Services injected</param>
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_2);
            services.AddApiVersioning(o =>  
            {  
                o.ReportApiVersions = true;  
                o.AssumeDefaultVersionWhenUnspecified = true;  
                o.DefaultApiVersion = new ApiVersion(1, 0);  
                o.ApiVersionReader = new UrlSegmentApiVersionReader();  
            });
            
            services.AddSwaggerGen(c =>
            {
                // Autogenerate API documentation from the code
                c.EnableAnnotations();
                var filePath = Path.Combine(AppContext.BaseDirectory, "Sonidos.API.xml");
                c.IncludeXmlComments(filePath);
                
                c.SwaggerDoc("v1", new Info 
                {
                    Title = "Sonidos API",
                    Version = "v1",
                    Description = "Use this API to retrieve albums and songs.",
                    TermsOfService = "http://tempuri.org/terms",
                    Contact = new Contact
                    {
                        Name = "Adrian Kosmaczewski",
                        Url = "https://akos.ma/",
                        Email = "akosma@me.com"
                    },
                    License = new License
                    {
                        Name = "BSD License",
                        Url = "https://akos.ma/"
                    }
                });
            });

            services.AddDbContext<SongsContext>();
        }

        /// <summary>
        /// This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        /// </summary>
        /// <param name="app">Application object</param>
        /// <param name="env">Hosting environment object</param>
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                // The default HSTS value is 30 days. You may want to change this for production scenarios, see https://aka.ms/aspnetcore-hsts.
                app.UseHsts();
            }

            //app.UseHttpsRedirection();
            app.UseMvc();
            app.UseSwagger();
            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Sonidos API V1");
            });
        }
    }
}