using System.Collections.Generic;
using Microsoft.AspNetCore.Mvc;
using Sonidos.Data.Contexts;
using Sonidos.Models;

namespace Sonidos.API.V1.Controllers
{
    /// <summary>
    /// API for returning album information.
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]  
    [Route("api/v1/albums")]
    public class AlbumsController : ControllerBase
    {
        private readonly SongsContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Database context</param>
        public AlbumsController(SongsContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Gets all the albums
        /// </summary>
        /// <returns>An action result wrapping an enumerable list of albums</returns>
        [HttpGet]
        public ActionResult<IEnumerable<Album>> Get()
        {
            return _context.Albums;
        }
    }
}