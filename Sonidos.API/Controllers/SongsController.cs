﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.FileProviders;
using Sonidos.Data.Contexts;
using Sonidos.Models;
using Sonidos.Tools.Extensions;

namespace Sonidos.API.V1.Controllers
{
    /// <summary>
    /// API returning songs to clients
    /// </summary>
    [ApiController]
    [ApiVersion("1.0")]  
    [Route("api/v1/songs")]
    [Description("API returning songs to clients")]
    public class SongsController : ControllerBase
    {
        private readonly SongsContext _context;

        /// <summary>
        /// Constructor
        /// </summary>
        /// <param name="context">Database context</param>
        public SongsController(SongsContext context)
        {
            _context = context;
        }
        
        /// <summary>
        /// Returns all the songs available in the system.
        /// </summary>
        /// <returns>A result with an enumerable with song objects</returns>
        [HttpGet]
        [ProducesResponseType(typeof(IEnumerable<Song>), 200)]
        public ActionResult<IEnumerable<Song>> AllSongs()
        {
            return _context.Songs
                .OrderBy(obj => obj.DisplayFilename)
                .ToList();
        }
        
        /// <summary>
        /// Download the song specified by the GUID passed as parameter
        /// </summary>
        /// <param name="guid">The GUID of the song</param>
        /// <returns>A FileResult with the specified file, of NotFound if none available</returns>
        [HttpGet]
        [Route("{guid}")]
        [ProducesResponseType(404)]
        [ProducesResponseType(typeof(Song), 200)]
        public IActionResult Download(Guid guid)
        {
            Song song = _context.Songs.FirstOrDefault(obj => obj.Id == guid);
            if (song == null)
            {
                return NotFound();
            }

            Guid storedFilename = song.Id;
            string displayFilename = song.DisplayFilename;

            return this.DownloadFile(storedFilename, displayFilename);
        }

//        // POST api/songs
//        [HttpPost]
//        public void Post([FromBody] string value)
//        {
//        }
//
//        // PUT api/songs/5
//        [HttpPut("{id}")]
//        public void Put(int id, [FromBody] string value)
//        {
//        }
//
//        // DELETE api/songs/5
//        [HttpDelete("{id}")]
//        public void Delete(int id)
//        {
//        }
    }
}