using System;
using Sonidos.Models;
using Xunit;

namespace Sonidos.Tests
{
    public class SongTests
    {
        [Fact]
        public void HasName()
        {
            const string FILENAME = "test.mp3";

            var song1 = new Song
            {
                DisplayFilename = FILENAME
            };
            
            Assert.Equal(FILENAME, song1.DisplayFilename);
        }
    }
}