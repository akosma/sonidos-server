using System;

namespace Sonidos.Models
{
    public class Song
    {
        public Guid Id { get; set; } = Guid.NewGuid();
        public string DisplayFilename { get; set; }
        public long Size { get; set; }
        public long Length { get; set; }
        public Album Album { get; set; }
    }
}