using System;
using System.Collections.Generic;

namespace Sonidos.Models
{
    public class Album
    {
        public Guid Id { get; set; }
        public string Name { get; set; }
        public DateTime PublicationDateTime { get; set; }
        public IEnumerable<Song> Songs { get; set; }
    }
}